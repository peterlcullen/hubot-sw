# Description:
#   Hubot thinks things are funny too....
#
# Dependencies:
#   None
#
# Configuration:
#   HUBOT_HAHA_WORDS - add a comma-delimited list of words
#
# Author:
#   peterlcullen

# if the config isn't setup, export a blank function and return out of this file early
# there's probably a more concise way to to this, but i don't know coffeescript .....
unless process.env.HUBOT_HAHA_WORDS
  module.exports = ->
  return

# create the regex
wordsArray = process.env.HUBOT_HAHA_WORDS.split ","
wordsString = wordsArray.join "|"
regexString = "\\b(" + wordsString + ")\\b"
regex = new RegExp regexString, "i"

chuckles = [
  "haha!",
  "hehe...",
  "LOL -"
]

module.exports = (robot) ->
  robot.hear regex, (msg) ->
    matchedText = msg.message.text.match(regex)
    if matchedText
      haha = msg.random chuckles
      msg.send haha + " you said " + matchedText[1]
